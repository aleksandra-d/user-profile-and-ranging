package com.capgemini.rest;

public class BoardGameErrorResponse {

    public BoardGameErrorResponse(String appName, String message) {
        this.appName = appName;
        this.message = message;
    }

    private String appName;
    private String message;

    public String getAppName() {
        return appName;
    }

    public BoardGameErrorResponse setAppName(String appName) {
        this.appName = appName;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public BoardGameErrorResponse setMessage(String message) {
        this.message = message;
        return this;
    }

    public BoardGameErrorResponse() {
    }


}
