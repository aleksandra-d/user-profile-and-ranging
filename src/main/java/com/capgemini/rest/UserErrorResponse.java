package com.capgemini.rest;

public class UserErrorResponse {
    private int status;
    private String message;
    private Long timeStamp;

    public UserErrorResponse() {
    }

    public UserErrorResponse(int status, String message, Long timeStamp) {
        this.status = status;
        this.message = message;
        this.timeStamp = timeStamp;
    }

    public int getStatus() {
        return status;
    }

    public UserErrorResponse setStatus(int status) {
        this.status = status;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public UserErrorResponse setMessage(String message) {
        this.message = message;
        return this;
    }

    public Long getTimeStamp() {
        return timeStamp;
    }

    public UserErrorResponse setTimeStamp(Long timeStamp) {
        this.timeStamp = timeStamp;
        return this;
    }
}
