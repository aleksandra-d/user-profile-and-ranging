package com.capgemini.rest;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class GlobalControllerAdvice {

    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalControllerAdvice.class);

    @ResponseBody
    @ExceptionHandler(BoardGameNotFoundException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public BoardGameErrorResponse gameNotFoundExceptionHandler(Exception ex) {
        LOGGER.error("This board game doesn't exist", ex);
        return new BoardGameErrorResponse("App001", ex.getMessage());

    }
}
