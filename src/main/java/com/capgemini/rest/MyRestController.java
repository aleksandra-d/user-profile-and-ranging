package com.capgemini.rest;


import com.capgemini.dao.UserDao;


import com.capgemini.dto.UserDto;
import com.capgemini.dto.UsersBoardGameDto;
import com.capgemini.service.UsersBoardGameService;
import com.capgemini.validator.UsersBoardGameValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class MyRestController {

    private UserDao userDao;
    private UsersBoardGameService usersBoardGameService;
    private UsersBoardGameValidator validator;

    @Autowired
    public MyRestController(UserDao userDao, UsersBoardGameService usersBoardGameService, UsersBoardGameValidator validator) {
        this.userDao = userDao;
        this.usersBoardGameService = usersBoardGameService;
        this.validator = validator;
    }


    @GetMapping("/user/{userId}")
    @ResponseBody
    public ResponseEntity<List<UsersBoardGameDto>> getBoardGamesByUserId(@PathVariable long userId) {
        List<UsersBoardGameDto> list = usersBoardGameService.displayUsersBoardGames(userId);
       if(userId < 0){
           throw new BoardGameNotFoundException("There is no such game for this user" + userId);
       }
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(list);
    }


    @PutMapping()
    public void  updateUserBoardGame(@RequestBody UsersBoardGameDto usersBoardGameDto) {
      validator.validate(usersBoardGameDto);

        usersBoardGameService.updateUserBoardGame(usersBoardGameDto);
    }


    @GetMapping("/search/{name}/{surname}/{email}")
    @ResponseBody
    private List<UserDto> search(@PathVariable String name, @PathVariable String surname, @PathVariable String email) {
        return userDao.findUserByNameSurnameEmail(name, surname, email);
    }

}




























