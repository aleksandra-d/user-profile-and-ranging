package com.capgemini.rest;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class BoardGameNotFoundException extends RuntimeException {

    public BoardGameNotFoundException(String message) {
        super(message);
    }

    public BoardGameNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public BoardGameNotFoundException(Throwable cause) {
        super(cause);
    }
}
