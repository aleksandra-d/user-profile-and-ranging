package com.capgemini.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class MyAspect {

    private static final Logger LOGGER = LoggerFactory.getLogger(MyAspect.class);

    @Around("execution (public void method*(..))")
    public Object timeMeasureAspect(ProceedingJoinPoint joinPoint) throws Throwable
    {
        long startTime = System.nanoTime();
        joinPoint.proceed();
        long endTime =System.nanoTime();
        long measuredTime = endTime - startTime;
        LOGGER.warn(String.valueOf(joinPoint.getSignature().getName()) + " " + String.valueOf(measuredTime));
        return joinPoint;
    }

}
