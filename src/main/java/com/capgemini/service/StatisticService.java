package com.capgemini.service;


import com.capgemini.dto.UsersChallengeDto;

import java.util.List;


public interface StatisticService {

    List<Object> checkUserStatistic(Long userId);

    UsersChallengeDto checkUserChallengeHistory(Long userId);
}
