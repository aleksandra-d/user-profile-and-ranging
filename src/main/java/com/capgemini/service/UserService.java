package com.capgemini.service;

import com.capgemini.dto.UserDto;

import java.util.Optional;

public interface UserService {
    Optional<UserDto> findUserById(Long userId);

}
