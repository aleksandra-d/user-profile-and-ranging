package com.capgemini.service;

import com.capgemini.dto.BoardGameDto;
import com.capgemini.dto.UsersBoardGameDto;

import java.util.List;


public interface UsersBoardGameService {
    List<UsersBoardGameDto> displayUsersBoardGames(Long userId);
    void updateUserBoardGame(UsersBoardGameDto usersBoardGameDto);
    Long removeGameFromUsersCollection(UsersBoardGameDto usersBoardGameDto);

    Long addGameToUsersCollection(Long boardGameId, Long userId);
    Long addGameToSystemCollection(BoardGameDto boardGameDto);

}