package com.capgemini.service.impl;

import com.capgemini.dao.UsersBoardGameDao;
import com.capgemini.dto.BoardGameDto;
import com.capgemini.dto.UsersBoardGameDto;
import com.capgemini.service.UsersBoardGameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BoardGamesServiceImpl implements UsersBoardGameService {

    private UsersBoardGameDao usersBoardGameDao;


    @Autowired
    public BoardGamesServiceImpl(UsersBoardGameDao usersBoardGameDao) {
        this.usersBoardGameDao = usersBoardGameDao;
    }

    @Override
    public List<UsersBoardGameDto> displayUsersBoardGames(Long userId) {
        return usersBoardGameDao.findUsersBoardGamesByUserId(userId);
    }

    @Override
    public void updateUserBoardGame(UsersBoardGameDto usersBoardGameDto){
        usersBoardGameDao.updateUserBoardGame(usersBoardGameDto);
    }

    @Override
    public Long removeGameFromUsersCollection(UsersBoardGameDto usersBoardGameDto) {
        return usersBoardGameDao.removeGameFromUsersCollection(usersBoardGameDto);
    }

    @Override
    public Long addGameToUsersCollection(Long boardGameId, Long userId) {
        return usersBoardGameDao.addGameToUsersCollection(boardGameId, userId);
    }

    @Override
    public Long addGameToSystemCollection(BoardGameDto boardGameDto) {
        return usersBoardGameDao.addGameToSystemCollection(boardGameDto);
    }

}
