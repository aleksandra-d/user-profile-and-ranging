package com.capgemini.service.impl;


import com.capgemini.dao.StatisticDao;
import com.capgemini.dao.UsersChallengeDao;
import com.capgemini.dto.StatisticDto;
import com.capgemini.dto.UsersChallengeDto;
import com.capgemini.service.StatisticService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class StatisticServiceImpl implements StatisticService {

    private StatisticDao statisticDao;

    private UsersChallengeDao usersChallengesDao;

    @Autowired
    public StatisticServiceImpl(StatisticDao statisticDao, UsersChallengeDao usersChallengesDao) {
        this.statisticDao = statisticDao;
        this.usersChallengesDao = usersChallengesDao;
    }

    @Override
    public List<Object> checkUserStatistic(Long userId) {
        Optional<StatisticDto> statistic = statisticDao.findByUserId(userId);
        List<Object> list = new ArrayList<>();
        if(statistic.isPresent()){
            list.add(statistic.get().getUsersRankingPosition());
            list.add(statistic.get().getUserLevel());
        }

        return list;
    }

    @Override
    public UsersChallengeDto checkUserChallengeHistory(Long userId) {
        return (UsersChallengeDto) usersChallengesDao.findChallengesByUserId(userId);
    }
}
