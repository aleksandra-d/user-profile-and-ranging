package com.capgemini.service.impl;

import com.capgemini.dao.UserDao;
import com.capgemini.dto.UserDto;
import com.capgemini.entity.User;
import com.capgemini.mapper.UserMapper;
import com.capgemini.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

public class UserServiceImpl implements UserService {
    private final UserDao userDao;

    private final UserMapper mapper;

    @Autowired
    public UserServiceImpl(UserDao userDao, UserMapper mapper) {
        this.userDao = userDao;
        this.mapper = mapper;
    }


    @Override
    public Optional<UserDto> findUserById(Long userId)  {
        Optional<User> foundUser = userDao.findUserById(userId);

        if (!foundUser.isPresent()) {
            throw new IndexOutOfBoundsException();
        } else
            return mapper.map(foundUser);

    }


    public void updateUser(UserDto user) {
        userDao.save(user);

    }

    public void addUser(UserDto user) {
        userDao.save(user);
        
    }

}

