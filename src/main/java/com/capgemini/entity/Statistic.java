package com.capgemini.entity;

import com.capgemini.enums.Level;

public class Statistic {

    private Long id;
    private static Long idIncrement = 0L;
    private Long userId;
    private Level userLevel;
    private Long usersRankingPosition;

    public Statistic(Long userId, Level level, Long usersRankingPosition) {
        this.id = idIncrement++;
        this.userId = userId;
        this.userLevel = level;
        this.usersRankingPosition = usersRankingPosition;
    }

    public Statistic() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public static Long getIdIncrement() {
        return idIncrement;
    }

    public static void setIdIncrement(Long idIncrement) {
        Statistic.idIncrement = idIncrement;
    }

    public Level getUserLevel() {
        return userLevel;
    }

    public void setUserLevel(Level userLevel) {
        this.userLevel = userLevel;
    }

    public Long getUsersRankingPosition() {
        return usersRankingPosition;
    }

    public void setUsersRankingPosition(Long usersRankingPosition) {
        this.usersRankingPosition = usersRankingPosition;
    }
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
