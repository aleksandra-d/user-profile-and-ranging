package com.capgemini.entity;

import java.time.LocalDate;

public class Playability {

    private Long id;
    private static Long increment = 0L;
    private Long userId;
    private LocalDate date;
    private LocalDate startDate;

    public Playability(Long userId, LocalDate date, LocalDate startDate, LocalDate endDate) {
        this.id = increment++;
        this.userId = userId;
        this.date = date;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    private LocalDate endDate;

}
