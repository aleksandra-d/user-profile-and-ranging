package com.capgemini.entity;

public class UsersBoardGame {

    private static Long increment = 0L;

    public Long getId() {
        return id;
    }

    private Long id;

    private Long userId;
    private Long boardGameId;

    public UsersBoardGame(Long userId, Long boardGameId) {
        this.id = increment++;
        this.userId = userId;
        this.boardGameId = boardGameId;
    }

    @Override
    public String toString() {
        return "UsersBoardGame{" +
                "id=" + id +
                ", userId=" + userId +
                ", boardGameId=" + boardGameId +
                '}';
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UsersBoardGame() {

    }

    public Long getUserId() {

        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getBoardGameId() {

        return boardGameId;
    }

    public void setBoardGameId(Long boardGameId) {

        this.boardGameId = boardGameId;
    }

}
