package com.capgemini.entity;

public class User {

    private Long id;
    private static Long idIncrement = 0L;
    private String name = "";

    public User(String name, String motto, String email) {
        this.id = idIncrement++;
        this.name = name;
        this.motto = motto;
        this.email = email;
    }

    private String surname = "";
    private String motto = "";
    private String password;
    private String email = "";
    private static Long winsIncrement = 0L;
    private Long winnings = 0L;

    public Long getWinnings() {
        return winnings;
    }

    public void setWinnings() {
        this.winnings = winsIncrement++;
    }

    /**
     * Constructor for User
     * */
    public User(String name, String surname, String motto, String password, String email) {
        this.id = idIncrement++;
        this.name = name;
        this.surname = surname;
        this.motto = motto;
        this.password = password;
        this.email = email;
    }

    public User() {
    }

    /**
     * Getters and setters
     * */
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getMotto() {
        return motto;
    }

    public void setMotto(String motto) {
        this.motto = motto;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public static User getUser() {
        return new User();
    }


}
