package com.capgemini.entity;

public class UsersChallenge {

    private Long id;
    private static Long increment = 0L;
    private Long challengeId;
    private Long userId;

    public UsersChallenge(Long challengeId, Long userId) {
        this.id = increment++;
        this.challengeId = challengeId;
        this.userId = userId;
    }

    public UsersChallenge() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getChallengeId() {
        return challengeId;
    }

    public void setChallengeId(Long challengeId) {
        this.challengeId = challengeId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
