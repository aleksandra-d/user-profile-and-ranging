package com.capgemini.entity;

public class Challenge {

    private Long id;
    private static Long idIncrement = 0L;
    private Long userId;
    private Long gameId;

    public Challenge(Long userId, Long gameId) {
        this.id = idIncrement++;
        this.userId = userId;
        this.gameId = gameId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getGameId() {
        return gameId;
    }

    public void setGameId(Long gameId) {
        this.gameId = gameId;
    }

}
