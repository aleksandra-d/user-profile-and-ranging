package com.capgemini.entity;

public class BoardGame {

    private Long id;
    private static Long increment =  0L;
    private String title = "";
    private int minimumPlayers;
    private int maximumPlayers;

    public BoardGame(String title, int minPlayers, int maxPlayers) {
        this.id = increment++;
        this.title = title;
        this.minimumPlayers = minPlayers;
        this.maximumPlayers = maxPlayers;
    }

    public BoardGame() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getMinimumPlayers() {
        return minimumPlayers;
    }

    public void setMinimumPlayers(int minimumPlayers) {
        this.minimumPlayers = minimumPlayers;
    }

    public int getMaximumPlayers() {
        return maximumPlayers;
    }

    public void setMaximumPlayers(int maximumPlayers) {
        this.maximumPlayers = maximumPlayers;
    }

}
