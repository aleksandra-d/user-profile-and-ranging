package com.capgemini.dto;

import com.capgemini.enums.Level;

public class StatisticDto {

    private Long id;
    private Long userId;
    private Level userLevel;
    private Long usersRankingPosition;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Level getUserLevel() {
        return userLevel;
    }

    public void setUserLevel(Level userLevel) {
        this.userLevel = userLevel;
    }

    public Long getUsersRankingPosition() {
        return usersRankingPosition;
    }

    public void setUsersRankingPosition(Long usersRankingPosition) {
        this.usersRankingPosition = usersRankingPosition;
    }
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
