package com.capgemini.dto;

import com.capgemini.entity.User;

public class UserDto {
    private Long id;
    private static Long idIncrement = 0L;
    private String name = "";
    private String surname = "";
    private String motto = "";
    private String password;
    private String email = "";


    public UserDto(String name, String surname, String motto, String password, String email) {
        this.id = idIncrement++;
        this.name = name;
        this.surname = surname;
        this.motto = motto;
        this.password = password;
        this.email = email;
    }

    public UserDto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getMotto() {
        return motto;
    }

    public void setMotto(String motto) {
        this.motto = motto;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public static User getUser() { // musi być static, żeby obsłuż
        return new User();
    }
}
