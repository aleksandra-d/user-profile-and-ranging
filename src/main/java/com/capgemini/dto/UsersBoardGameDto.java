package com.capgemini.dto;

public class UsersBoardGameDto {

    private static Long increment = 0L;

    public UsersBoardGameDto(Long userId, Long boardGameId) {
        this.id = increment++;
        this.userId = userId;
        this.boardGameId = boardGameId;
    }

    public UsersBoardGameDto() {

    }

    @Override
    public String toString() {
        return "UsersBoardGameDto{" +
                "id=" + id +
                ", userId=" + userId +
                ", boardGameId=" + boardGameId +
                '}';
    }

    private Long id;
    private Long userId;
    private Long boardGameId;

    public Long getId() {
        return id;
    }
    public void setUserId(Long userId) {
        this.userId = userId;
    }


    public Long getUserId() {

        return userId;
    }

    public Long getBoardGameId() {

        return boardGameId;
    }

    public void setBoardGameId(Long boardGameId) {
        this.boardGameId = boardGameId;
    }

}

