package com.capgemini.validator;

import com.capgemini.dto.BoardGameDto;

import com.capgemini.entity.BoardGame;
import com.capgemini.rest.BoardGameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class BoardGameValidator {
    public boolean validate(BoardGameDto boardGameDto) {
        if (boardGameDto == null) {
            throw new BoardGameNotFoundException("board game not found");
        }
        return true;
    }

    public boolean isGameInSystem(List<BoardGame> systemBoardGames, Long gameId){
        if(systemBoardGames.stream().anyMatch(game -> game.getId().equals(gameId))){
            return true;
        }
        else throw new BoardGameNotFoundException("board game not found");

    }
}
