package com.capgemini.validator;

import com.capgemini.dto.UsersChallengeDto;
import com.capgemini.rest.BoardGameNotFoundException;
import org.springframework.stereotype.Component;

@Component
public class UsersChallengeValidator {

    public boolean validate(UsersChallengeDto usersChallengeDto) {
        if (usersChallengeDto == null) {
            throw new BoardGameNotFoundException("board game not found");
        }
        return true;
    }
}
