package com.capgemini.validator;


import com.capgemini.dto.UserDto;

import com.capgemini.rest.UserNotFoundException;
import org.springframework.stereotype.Component;

@Component
public class UserValidator {

    public boolean validate(UserDto userDto) {
        if (userDto == null) {
            throw new UserNotFoundException("user not found");
        }
        return true;
    }
}
