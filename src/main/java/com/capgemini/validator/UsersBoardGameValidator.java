package com.capgemini.validator;

import com.capgemini.dto.UsersBoardGameDto;

import com.capgemini.rest.BoardGameNotFoundException;

import org.springframework.stereotype.Component;

@Component
public class UsersBoardGameValidator {


    public boolean validate(UsersBoardGameDto usersBoardGameDto) {
        if (usersBoardGameDto == null) {
            throw new BoardGameNotFoundException("board game not found");
        }
        return true;
    }


}
