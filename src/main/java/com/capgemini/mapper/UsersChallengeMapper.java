package com.capgemini.mapper;

import com.capgemini.dto.UsersChallengeDto;
import com.capgemini.entity.UsersChallenge;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.capgemini.validator.UsersChallengeValidator;

@Component
public class UsersChallengeMapper {

    @Autowired
    public UsersChallengeMapper(UsersChallengeValidator validator) {
        this.validator = validator;
    }

    private UsersChallengeValidator validator;

    public UsersChallengeDto map(UsersChallenge usersChallange)  {
        UsersChallengeDto usersChallengeDto = new UsersChallengeDto();
        if(validator.validate(usersChallengeDto)){
        usersChallengeDto.setChallengeId(usersChallange.getChallengeId());
        usersChallengeDto.setUserId(usersChallange.getUserId());
        }
        return usersChallengeDto;
    }

    public UsersChallenge map(UsersChallengeDto usersChallengeDto) {
        UsersChallenge usersChallenge = new UsersChallenge();
        setUsersChallengeIdIfPresent(usersChallengeDto, usersChallenge);
        usersChallenge.setChallengeId(usersChallengeDto.getChallengeId());
        usersChallenge.setUserId(usersChallengeDto.getChallengeId());


        return usersChallenge;
    }

    private void setUsersChallengeIdIfPresent(UsersChallengeDto usersChallengeDto, UsersChallenge usersChallenge) {
        if (usersChallengeDto.getId() != null) {
            usersChallenge.setId(usersChallengeDto.getId());
        }
    }
}
