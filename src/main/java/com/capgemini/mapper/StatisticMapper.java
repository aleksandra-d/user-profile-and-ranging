package com.capgemini.mapper;

import com.capgemini.dto.StatisticDto;
import com.capgemini.entity.Statistic;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class StatisticMapper {


    public Optional<StatisticDto> map(Optional<Statistic> statistic) {
        StatisticDto statisticDto = new StatisticDto();
        if (statistic.isPresent()) {
            statisticDto.setUserId(statistic.get().getUserId());
            statisticDto.setUserLevel(statistic.get().getUserLevel());
            statisticDto.setUsersRankingPosition(statistic.get().getUsersRankingPosition());
        }
        return Optional.of(statisticDto);
    }

    public Statistic map(StatisticDto statisticDto) {
        Statistic statistic = new Statistic();
        setStatisticIdIfPresent(statisticDto, statistic);
        statistic.setUserId(statisticDto.getUserId());
        statistic.setUserLevel(statisticDto.getUserLevel());
        statistic.setUsersRankingPosition(statisticDto.getUsersRankingPosition());

        return statistic;
    }

    private void setStatisticIdIfPresent(StatisticDto statisticDto, Statistic statistic) {
        if (statisticDto.getId() != null) {
            statistic.setId(statisticDto.getId());
        }
    }

}