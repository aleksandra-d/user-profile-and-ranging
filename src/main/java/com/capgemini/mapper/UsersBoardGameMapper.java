package com.capgemini.mapper;

import com.capgemini.dto.UsersBoardGameDto;
import com.capgemini.entity.UsersBoardGame;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.capgemini.validator.UsersBoardGameValidator;

@Component
public class UsersBoardGameMapper {

    @Autowired
    private UsersBoardGameValidator validator;



    public UsersBoardGameDto map(UsersBoardGame userBoardGame){

        UsersBoardGameDto userBoardGameDto = new UsersBoardGameDto();
        userBoardGameDto.setBoardGameId(userBoardGame.getBoardGameId());
        userBoardGameDto.setUserId(userBoardGame.getUserId());

      validator.validate(userBoardGameDto);

        return userBoardGameDto;
    }

    public UsersBoardGame map(UsersBoardGameDto userBoardGameDto) {
        UsersBoardGame usersBoardGame = new UsersBoardGame();
        setUsersBoardGameIdIfPresent(userBoardGameDto, usersBoardGame);
        usersBoardGame.setBoardGameId(userBoardGameDto.getBoardGameId());
        usersBoardGame.setUserId(userBoardGameDto.getUserId());
        return usersBoardGame;
    }

    private void setUsersBoardGameIdIfPresent(UsersBoardGameDto usersBoardGameDto, UsersBoardGame usersBoardGame) {
        if (usersBoardGameDto.getId() != null) {
            usersBoardGame.setId(usersBoardGameDto.getId());
        }
    }

}

