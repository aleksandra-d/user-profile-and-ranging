package com.capgemini.mapper;

import com.capgemini.dto.UserDto;
import com.capgemini.entity.User;
import com.capgemini.validator.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class UserMapper {

    private UserValidator validator;

    @Autowired
    public UserMapper(UserValidator validator) {
        this.validator = validator;
    }

    public Optional<UserDto> map(Optional<User> user) {
        UserDto userDto = new UserDto();
        if (user.isPresent()) {
            userDto.setEmail(user.get().getEmail());
            userDto.setMotto(user.get().getMotto());
            userDto.setName(user.get().getName());
            userDto.setPassword(user.get().getPassword());
            userDto.setSurname(user.get().getSurname());
        }
        return Optional.of(userDto);
    }

    public UserDto map(User user) {
        UserDto userDto = new UserDto();

            userDto.setEmail(user.getEmail());
            userDto.setMotto(user.getMotto());
            userDto.setName(user.getName());
            userDto.setPassword(user.getPassword());
            userDto.setSurname(user.getSurname());

         validator.validate(userDto);
        return userDto;
    }
    public User map(UserDto userDto) {
        User user = new User();
        setUserIdIfPresent(userDto, user);
        user.setEmail(userDto.getEmail());
        user.setMotto(userDto.getMotto());
        user.setName(userDto.getName());
        user.setPassword(userDto.getPassword());
        user.setSurname(userDto.getSurname());

        return user;
    }

    private void setUserIdIfPresent(UserDto userDto, User user) {
        if (userDto.getId() != null) {
            user.setId(userDto.getId());
        }
    }
}
