package com.capgemini.mapper;

import com.capgemini.dto.BoardGameDto;

import com.capgemini.entity.BoardGame;

import com.capgemini.validator.BoardGameValidator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BoardGameMapper {

    private BoardGameValidator validator;

    @Autowired
    public BoardGameMapper(BoardGameValidator validator) {
        this.validator = validator;
    }


    public BoardGameDto map(BoardGame boardGame) {
        BoardGameDto boardGameDto = new BoardGameDto();
        if(validator.validate(boardGameDto)){
            boardGameDto.setMaximumPlayers(boardGame.getMaximumPlayers());
            boardGameDto.setMinimumPlayers(boardGame.getMinimumPlayers());
            boardGameDto.setTitle(boardGame.getTitle());
        }
        return boardGameDto;
    }

    public BoardGame map(BoardGameDto boardGameDto) {
        BoardGame boardGame = new BoardGame();
        setBoardGameIdIfPresent(boardGameDto, boardGame);
        boardGame.setMaximumPlayers(boardGameDto.getMaximumPlayers());
        boardGame.setMinimumPlayers(boardGameDto.getMinimumPlayers());
        boardGame.setTitle(boardGameDto.getTitle());

        return boardGame;
    }

    private void setBoardGameIdIfPresent(BoardGameDto boardGameDto, BoardGame boardGame) {
        if (boardGameDto.getId() != null) {
            boardGame.setId(boardGameDto.getId());
        }
    }

}
