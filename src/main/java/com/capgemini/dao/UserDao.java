package com.capgemini.dao;

import com.capgemini.dto.UserDto;
import com.capgemini.entity.User;

import java.util.List;
import java.util.Optional;

public interface UserDao {
    Optional<User> findUserById(Long id);
    Long save(UserDto userDto);
    List<UserDto> findUserByNameSurnameEmail(String name, String surname, String email);
}
