package com.capgemini.dao.impl;

import com.capgemini.dto.StatisticDto;
import com.capgemini.entity.Statistic;
import com.capgemini.enums.Level;
import com.capgemini.mapper.StatisticMapper;
import com.capgemini.dao.StatisticDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class StatisticDaoImpl implements StatisticDao {

    private StatisticMapper mapper;
    private static List<Statistic> statistics;

    static {
        resetStatistics();
    }

    private static void resetStatistics() {
        statistics = new ArrayList<>();
        statistics.add(new Statistic(1L, Level.JUNIOR, 1L));
        statistics.add(new Statistic(2L, Level.MEDIUM, 2L));
        statistics.add(new Statistic(3L, Level.SENIOR, 3L));
    }

    @Autowired
    public StatisticDaoImpl(StatisticMapper mapper) {
        this.mapper = mapper;
    }

    @Override
    public Long save(StatisticDto statisticDto) {
        Statistic statistic = mapper.map(statisticDto);
        Optional<Statistic> foundStatistic = findStatisticById(statistic.getId());
        if (foundStatistic.isPresent()) {
            statistics.remove(foundStatistic.get());
            statistic = foundStatistic.get();
        }
        statistics.add(statistic);
        return statistic.getId();
    }

    @Override
    public Optional<StatisticDto> findByUserId(Long userId) {
        Optional<Statistic> statistic = findStatisticByUserId(userId);
        return mapper.map(statistic);
    }

    @Override
    public Optional<StatisticDto> findById(Long id) {
        Optional<Statistic> statistic = findStatisticById(id);
        return mapper.map(statistic);
    }

    private Optional<Statistic> findStatisticById(Long id) {
        return statistics.stream().filter(s -> s.getId().equals(id)).findFirst();
    }

    private Optional<Statistic> findStatisticByUserId(Long userId) {
        return statistics.stream().filter(s -> s.getUserId().equals(userId)).findFirst();
    }
}


