package com.capgemini.dao.impl;

import com.capgemini.dao.UserDao;
import com.capgemini.dto.UserDto;
import com.capgemini.entity.User;
import com.capgemini.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class UserDaoImpl implements UserDao {

    private static List<User> users;
    private UserMapper mapper;

    static {
        resetUsers();
    }

    @Autowired
    public UserDaoImpl(UserMapper mapper) {
        this.mapper = mapper;
    }

    private static void resetUsers() {
        users = new ArrayList<>();
        //String name, String surname, String motto, String password, String email
        users.add(new User("name", "surname", "motto", "password", "email"));
        users.add(new User("name1", "surname1", "motto1", "password1", "email1"));
        users.add(new User("name2", "surname2", "motto2", "password2", "email2"));
    }

    @Override
    public Optional<User> findUserById(Long userId) {
        return users.stream().filter(user -> user.getId().equals(userId)).findFirst();

    }

    @Override
    public Long save(UserDto userDto) {

        User user = mapper.map(userDto);
        Optional<User> foundUser = findUserById(user.getId());
        if (foundUser.isPresent()) {
            users.remove(foundUser.get());
        }
        users.add(user);

        return user.getId();
    }

    public List<UserDto> findUserByNameSurnameEmail(String name, String surname, String email) {

        return users.stream()
                .filter(user ->
                        user.getName() == null ||
                                user.getName().isEmpty() ||
                                user.getName().equalsIgnoreCase(name))

                .filter(user ->
                        user.getSurname() == null ||
                                user.getSurname().isEmpty() ||
                                user.getName().equalsIgnoreCase(surname))

                .filter(user ->
                        user.getEmail() == null ||
                                user.getEmail().isEmpty() ||
                                user.getEmail().equalsIgnoreCase(email))
                .map(user -> mapper.map(user))
                .collect(Collectors.toList());
    }
}