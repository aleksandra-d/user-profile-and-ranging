package com.capgemini.dao.impl;

import com.capgemini.dao.UsersChallengeDao;
import com.capgemini.dto.UsersChallengeDto;
import com.capgemini.entity.UsersChallenge;
import com.capgemini.mapper.UsersChallengeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class UsersChallengesDaoImpl implements UsersChallengeDao {

    private UsersChallengeMapper mapper;

    private static List<UsersChallenge> usersChallenges;


    static {
        resetUsersChallanges();
    }

    private static void resetUsersChallanges() {
        usersChallenges = new ArrayList<>();
        usersChallenges.add(new UsersChallenge(0L, 1L));
        usersChallenges.add(new UsersChallenge(1L, 1L));
    }


    @Autowired
    public UsersChallengesDaoImpl(UsersChallengeMapper mapper) {

        this.mapper = mapper;
    }

    @Override
    public List<UsersChallengeDto> findChallengesByUserId(Long userId) {

        return findChallangesIdByUserId(userId);
    }

    private List<UsersChallengeDto> findChallangesIdByUserId(Long userId) {
        return usersChallenges
                .stream()
                .filter(userChallenge -> userChallenge.getUserId().equals(userId))
                .map(userChallenge -> mapper.map(userChallenge)).collect(Collectors.toList());
    }
}
