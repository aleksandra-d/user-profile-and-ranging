package com.capgemini.dao.impl;

import com.capgemini.dao.UsersBoardGameDao;
import com.capgemini.dto.BoardGameDto;
import com.capgemini.dto.UsersBoardGameDto;
import com.capgemini.entity.BoardGame;
import com.capgemini.entity.UsersBoardGame;
import com.capgemini.mapper.BoardGameMapper;
import com.capgemini.mapper.UsersBoardGameMapper;
import com.capgemini.validator.BoardGameValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class UsersBoardGameDaoImpl implements UsersBoardGameDao {

    private UsersBoardGameMapper userBoardGameMapper;
    private BoardGameMapper boardGameMapper;
    private BoardGameValidator validator;

    private static List<UsersBoardGame> usersBoardGames;

    public static List<BoardGame> getSystemBoardGames() {
        return systemBoardGames;
    }

    public static void setSystemBoardGames(List<BoardGame> systemBoardGames) {
        UsersBoardGameDaoImpl.systemBoardGames = systemBoardGames;
    }

    private static List<BoardGame> systemBoardGames;

    static {
        resetUsersBoardGames();
    }

    @Autowired
    public UsersBoardGameDaoImpl(UsersBoardGameMapper userBoardGameMapper, BoardGameMapper boardGameMapper, BoardGameValidator validator) {
        this.userBoardGameMapper = userBoardGameMapper;
        this.boardGameMapper = boardGameMapper;
        this.validator = validator;
    }

    private static void resetUsersBoardGames() {
        usersBoardGames = new ArrayList<>();
        usersBoardGames.add(new UsersBoardGame(0L, 0L));
        usersBoardGames.add(new UsersBoardGame(1L, 1L));
        usersBoardGames.add(new UsersBoardGame(0L, 2L));

    }

    static {
        resetBoardGames();
    }

    private static void resetBoardGames() {
        systemBoardGames = new ArrayList<>();
        systemBoardGames.add(new BoardGame("osadnicy", 2, 6));
        systemBoardGames.add(new BoardGame("siedem cudów", 1, 7));
        systemBoardGames.add(new BoardGame("colt express", 2, 5));

    }

    @Override
    public void updateUserBoardGame(UsersBoardGameDto usersBoardGameDto) {

        Optional<UsersBoardGame> boardGame = findUsersGameById(usersBoardGameDto.getUserId());
        if (boardGame.isPresent()) {

            boardGame.get().setUserId(usersBoardGameDto.getUserId());
            boardGame.get().setBoardGameId(usersBoardGameDto.getBoardGameId());

        }
    }

    @Override
    public List<UsersBoardGameDto> findUsersBoardGamesByUserId(Long userId) {
        return findUsersBoardGames(userId);
    }

    @Override
    public Long removeGame(Long boardGameId) {
        Optional<UsersBoardGame> usersBoardGame = findUsersGameById(boardGameId);
        UsersBoardGame foundUserBoardGame = new UsersBoardGame();
        if (usersBoardGame.isPresent()) {
            foundUserBoardGame = usersBoardGame.get();
        }
        usersBoardGames.remove(foundUserBoardGame);
        return foundUserBoardGame.getId();
    }

    @Override
    public Long removeGameFromUsersCollection(UsersBoardGameDto usersBoardGameDto) {
        UsersBoardGame game = userBoardGameMapper.map(usersBoardGameDto);
        Long id = game.getBoardGameId();
        Optional<UsersBoardGame> foundGame = usersBoardGames.stream().filter(g -> g.getBoardGameId().equals(id)).findFirst();
        if (foundGame.isPresent()) {
            usersBoardGames.remove(foundGame.get());
        }

        return id;
    }


    @Override
    public Long addGameToUsersCollection(Long boardGameId, Long userId) {

        UsersBoardGame usersBoardGame = new UsersBoardGame();
        usersBoardGame.setBoardGameId(boardGameId);
        usersBoardGame.setUserId(userId);
        usersBoardGames.add(usersBoardGame);

        return usersBoardGame.getId();
    }

    @Override
    public Long addGameToSystemCollection(BoardGameDto boardGameDto) {
        BoardGame boardGame = boardGameMapper.map(boardGameDto);
        if(!validator.isGameInSystem(systemBoardGames, boardGame.getId())){
            systemBoardGames.add(boardGame);
        }
        return boardGame.getId();
    }


    private List<UsersBoardGameDto> findUsersBoardGames(Long userId) {
        return usersBoardGames
                .stream()
                .filter(usersBoardGame -> usersBoardGame.getUserId().equals(userId))
                .map(usersBoardGame -> userBoardGameMapper.map(usersBoardGame))
                .collect(Collectors.toList());
    }

    @Override
    public List<BoardGameDto> findSystemBoardGames() {
        return systemBoardGames
                .stream()
                .map(boardGame -> boardGameMapper.map(boardGame)).collect(Collectors.toList());
    }

    private Optional<UsersBoardGame> findUsersGameById(Long boardGameId) {
        return usersBoardGames
                .stream()
                .filter(usersBoardGame -> usersBoardGame.getBoardGameId().equals(boardGameId)).findFirst();
    }

    private Optional<BoardGame> findGameById(Long boardGameId) {
        return systemBoardGames
                .stream()
                .filter(boardGame -> boardGame.getId().equals(boardGameId)).findFirst();
    }
}
