package com.capgemini.dao;

import com.capgemini.dto.UsersChallengeDto;

import java.util.List;

public interface UsersChallengeDao {

    List<UsersChallengeDto> findChallengesByUserId(Long userId);

}
