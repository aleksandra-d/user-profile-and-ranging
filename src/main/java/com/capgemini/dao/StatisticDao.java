package com.capgemini.dao;

import com.capgemini.dto.StatisticDto;


import java.util.Optional;

public interface StatisticDao {

    Optional<StatisticDto> findByUserId(Long userId);
    Optional<StatisticDto> findById(Long id);
    Long save(StatisticDto statisticDto);
}
