package com.capgemini.dao;

import com.capgemini.dto.BoardGameDto;
import com.capgemini.dto.UsersBoardGameDto;

import java.util.List;


public interface UsersBoardGameDao {

    List<UsersBoardGameDto> findUsersBoardGamesByUserId(Long userId);
    List<BoardGameDto> findSystemBoardGames();
    void updateUserBoardGame(UsersBoardGameDto usersBoardGameDto);
    Long removeGame(Long gameId);
    Long removeGameFromUsersCollection(UsersBoardGameDto usersBoardGameDto);
    Long addGameToUsersCollection(Long boardGameId, Long userId);
    Long addGameToSystemCollection(BoardGameDto boardGameDto);


}
