package com.capgemini.userprofileandranking;

import com.capgemini.dto.UsersBoardGameDto;
import com.capgemini.rest.MyRestController;
import com.capgemini.service.UsersBoardGameService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@WebAppConfiguration
public class MyNewRestControllerTest {

    private MockMvc mockMvc;

    @Mock
    private UsersBoardGameService usersBoardGameService;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private MyRestController myRestController;


    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        Mockito.reset(usersBoardGameService);
        mockMvc = MockMvcBuilders
                .webAppContextSetup(webApplicationContext)
                .build();
        ReflectionTestUtils.setField(myRestController, "usersBoardGameService", usersBoardGameService);

    }

    @Test
    public void testGetUserGames() throws Exception {
        //given
        Long userId = 0L;
        UsersBoardGameDto userGame = new UsersBoardGameDto(0L, 2L);
        UsersBoardGameDto userGameTwo = new UsersBoardGameDto(0L, 5L);
        List<UsersBoardGameDto> gameCollection = Arrays.asList(userGame, userGameTwo);
        Mockito.when(usersBoardGameService.displayUsersBoardGames(userId)).thenReturn(gameCollection);
        String strId = Long.toString(userId);
        //when
        ResultActions resultActions = mockMvc.perform(get("/api/user/{userId}", strId)
                .contentType(MediaType.APPLICATION_JSON));
        //then
        resultActions.andExpect(status().isOk()).
                andExpect(content()
                        .json("[{'id':null,'userId':0,'boardGameId':0}," +
                                "{'id':null,'userId':0,'boardGameId':2}]"));
    }

}
