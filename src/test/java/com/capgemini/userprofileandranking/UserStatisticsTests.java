package com.capgemini.userprofileandranking;


import com.capgemini.dao.impl.StatisticDaoImpl;
import com.capgemini.dto.StatisticDto;
import com.capgemini.entity.User;
import com.capgemini.enums.Level;
import com.capgemini.service.impl.StatisticServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;


@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class UserStatisticsTests {

    @Mock
    private StatisticDaoImpl statisticDao;

    @InjectMocks
    private StatisticServiceImpl statisticServiceImplMock;


    @Test
    public void shouldCheckUserStatistic() {
        //given
        User user = buildUser();
        Long userId = user.getId();
        StatisticDto statisticDto = new StatisticDto();
        statisticDto.setUserLevel(Level.JUNIOR);
        statisticDto.setUserId(1L);
        statisticDto.setUsersRankingPosition(1L);
        Mockito.when(statisticDao.findByUserId(userId)).thenReturn(Optional.of(statisticDto));

        //when
        List<Object> userStatistic = statisticServiceImplMock.checkUserStatistic(userId);

        //then
        assertThat(userStatistic, notNullValue());
        assertThat(userStatistic.get(0), is(1L));

    }


    private User buildUser() {
        User user = new User();
        user.setId(1L);
        user.setEmail("email");
        user.setMotto("jest fajnie");
        user.setSurname("kowalski");
        user.setName("jan");
        user.setPassword("1234");
        return user;
    }

}
